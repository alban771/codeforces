#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vs = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());  
  if ( v.size() ) 
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n,c,d;
  cin >> n >> c >> d;
  vector<long> b(n*n);
  cin >> b;
  vector<bool> a(n*n, false);

  long a11 = std::reduce(b.begin(), b.end(), numeric_limits<long>::max(),
      [](long lhs, long rhs){ return min(lhs,rhs); });
  for ( long bij : b ) {
    bij -= a11;
    bool f = false;
    for( int i = 0; i < n && !f; ++i, bij -= c )
    {
      if ( bij % d == 0 ) {
        int j = bij / d;
        if ( j >= 0 && j < n && !a[i*n + j] ) {
          a[i*n+j] = true;
          f = true;
        }
      }
    }
    if ( !f ) return false;
  }

  return true;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
