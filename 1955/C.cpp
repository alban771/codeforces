#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vs = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  if ( v.size() == 0 ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }

using ll = long long;


auto solve() {
  int n;
  ll k;
  cin >> n >> k;
  vector<ll> a(n), b(n);
  cin >> a;
  int i = 0, j = n-1;
  bool lf;
  while( k >= 0 && i < j ) {
    if ( a[i] <= a[j] ) {
      k -= 2*a[i];
      a[j] -= a[i];
      ++i;
      lf = true;
    }
    else {
      k -= 2*a[j];
      a[i] -= a[j];
      --j;
      lf = false;
    }
  }
  if ( k >= 0 && i == j ) {
    k -= a[i];
    ++i;
    lf = false;
  }

  return i + n - j - 1 - (k < -lf);
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
