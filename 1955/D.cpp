#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;


template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n,m,k;
  cin >> n >> m >> k;
  vector<int> a(n), b(m);
  multiset<int> c, d, e;
  cin >> a >> b;

  rn::sort(b);
  rn::copy(a | vw::take(m), inserter(c, c.begin()));
  rn::set_intersection(b, c, inserter(d, d.begin()));
  rn::set_difference(b, d, inserter(e, e.begin()));
  for ( auto i = c.begin(), j = d.begin(); j != d.end(); ) {
    if ( *i == *j ) {
      auto e = i;
      ++i;
      c.erase(e);
      ++j;
    }
    else {
      ++i;
    }
  }
  int rv = d.size() >= k;

  for( int i = 0; i < n-m; ++i )
  {
    if ( auto cf = c.find(a[i]); cf != c.end() ) {
      c.erase(cf);
    }
    else {
      d.erase(d.find(a[i]));
      e.insert(a[i]);
    }
    if ( auto ef = e.find(a[i+m]); ef != e.end() ) {
      e.erase(ef);
      d.insert(a[i+m]);
    }
    else {
      c.insert(a[i+m]);
    }

    rv += d.size() >= k;
  }
  return rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
