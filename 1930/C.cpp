#include <bits/stdc++.h>
using namespace std;
using ull = unsigned long long;
namespace rn = std::ranges;
namespace vw = std::ranges::views;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }



auto solve() {
  vector<ull> a;
  cin >> a;

  for( int i = 0; i < a.size(); ++i ) {
    a[i] += i+1;
  }

  rn::sort(a, greater{});
  auto f = a.begin(), g = f;
  while ( f != a.end() ) {
    g = find_if( f+1, a.end(), [m=(*f)+1](auto e) mutable {
          return e < --m; });
    generate( f+1, g, [u=*f]() mutable { return --u; });
    f = g;
  }
  return a;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
