#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vs = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());  
  if ( v.size() ) 
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }



auto solve() {
  int _;
  string ln;
  vector<int> v;

  (cin >> _).ignore();
  getline(cin, ln);
  cerr << "input: " << ln << endl;

  auto ff = [](char e){ return e != ' '; };
  auto ln_f = ln | vs::filter(ff);
  auto ln_p = rn::subrange( ln.begin(), 
      rn::find_if_not( ln_f, 
      [m='0'](char e) mutable { 
        if ( e < m ) return false; else m = e; return true; }).base());

//  cerr << "stand out digit: " << *ln_p.end() << endl;

//  last number is always removed
  auto ln_m = ln_p
    | vs::reverse
    | vs::drop_while(ff);

//  cerr << "sorted digits: " << ln.substr(0, ln_m.size()) << endl;

  rn::transform( ln_m | vs::filter(ff), back_inserter(v), 
      [](char e){ return e - '0'; });
  rn::reverse(v);

  ln.erase(0, ln_m.size());
  istringstream is( ln );

  rn::copy(rn::subrange( istream_iterator<int>(is), 
        [&is](auto e){ return !is.good(); }), back_inserter(v));

//  cerr << "v: " << v << endl;
  return rn::is_sorted( v );
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
