#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vs = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());  
  if ( v.size() ) 
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }



vector<int> bfs(const vector<vector<int>> &adj, int root, size_t n) {
  vector<bool> vis(n, false); 
  vector<int> d;
  queue<int> q;
  q.push(root);
  d[root] = 0;
  vis[root] = true;

  while (q.size()) {
    int s = q.front();
    q.pop();
    for (int e : adj[s]) {
      if (vis[e])
        continue;
      q.push(e);
      vis[e] = true;
      d[e] = d[s] + 1;
    }
  }

  return d;
}

auto solve() {
  
  return;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
