#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const int v ){ cout << ( v == 2 ? "YES": v == 1 ? "MAYBE" : "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n,f,k;
  cin >> n >> f >> k;
  vector<int> a(n);
  cin >> a;
  if ( k == n ) return 2;
  int af = a[f-1];
//  cerr << "f: " << f << " k: " << k << endl;
//  cerr << "a: " << a << endl;
  rn::nth_element(a, a.begin()+(k-1), greater{} );
//  cerr << "a: " << a << endl;
  if ( af < a[k-1] ) 
    return 0;
  if ( af > a[k-1] )
    return 2;

  if ( rn::find(a | vw::drop(k), af) != a.end() )
    return 1;
  return 2;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
