#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  ll n,m;
  int k;
  cin >> n >> m >> k;
  vector<tuple<ll,ll,int>> v(k);
  rn::for_each(v, [i=-1] ( auto& p ) mutable {
      cin >> get<1>(p) >> get<0>(p); get<2>(p) = ++i; });
  rn::sort(v, [](const auto& lhs, const auto& rhs){ 
      auto [x1,y1,_] = lhs;
      auto [x2,y2,__] = rhs;
      return x1 < x2 || (x1 == x2 && y1 > y2); });
  ll h=0, w=1, a=0;
  vector<int> b(k,0);
  for( auto [x,y,i] : v ) {
    if (x == 1 || x > w) {
      a += (x-w) * (n-h);
      w = x;
      if ( y > h ) {
        h = y;
        b[i] = 1;
      }
    }
    ++i;
  }
  a += (m-w+1) * (n-h);

  cout << a << endl;
  return b;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
