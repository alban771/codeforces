#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }


template<typename T>
using mat = vector<vector<T>>;
using hash_t = ull;

template<typename T>
const auto vec_hash_1 = [](const auto &x) -> T{
      return reduce(x.begin(), x.end(), T{});
};

template<typename T>
const auto vec_hash_2 = [](const auto &x) -> T{
      return accumulate(x.begin(), x.end(), T{}, [](ull a, ull b){ return a + b*b; });
};

template<typename T, typename H1, typename H2>
pair<T,T> mat_hash( const mat<T>& a, H1&& h1 = {}, H2&& h2 = {}) {
  int n = a.size();
  int m = a[0].size();
  vector<T> h(n), w(m);

  rn::transform(a, h.begin(), H1{});
  
  for( int i = 0; i < m; ++i ) {
    vector<ull> x(a.size());
    rn::transform(a, x.begin(), [i](auto& r){ return r[i]; });
    w[i] = h1(x);
  }

  return {h2(h), h2(w)};
}


auto solve() {
  int n,m;
  cin >> n >> m;
  vector<ull> r0(m);
  mat<ull> a(n,r0), b(n,r0);
  const auto gtor = []( auto &r ){ cin >> r; };
  rn::for_each(a, gtor);
  rn::for_each(b, gtor);

//  cerr << "n: " << n << " m: " << m << endl;
//  cerr << "a: " << a << endl;
//  cerr << "b: " << b << endl;

  auto [ah,aw] = mat_hash(a, vec_hash_2<ull>, vec_hash_2<ull>);
  auto [bh,bw] = mat_hash(b, vec_hash_2<ull>, vec_hash_2<ull>);

  return ah == bh && aw == bw;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
