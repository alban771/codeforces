#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }


const auto strict_greater = [](ull u, ull v){ return u > v; };


auto solve() {
  int n;
  cin >> n;
  vector<ull> a(n), b(n-1), c(n-2);
  cin >> a;
  rn::generate(b, [&a,i=-1]() mutable { ++i; return gcd(a[i], a[i+1]); });
  cerr << "a: " << a << endl;
  cerr << "b: " << b << endl;

  auto f = rn::adjacent_find( b, strict_greater );
  if (f == b.end() || f+2 == b.end()) 
    return true;

  int k = f-b.begin();
  if ( f != b.begin() ) {
    copy(b.begin(), f-1, c.begin());
  }
  copy(f+1, b.end(), c.begin()+k);

  cerr << "k: " << k << endl;
  for( int i = 0; i < 3; ++i, ++k ) {
    // remove a[k], ignore b[k-1]
    if ( k > 1 )
      c[k-2] = b[k-2];
    if ( k > 0 && k < n-1 )
      c[k-1] = gcd(a[k-1], a[k+1]);
    cerr << "i: " << i << " c: " << c << endl;
    if ( rn::adjacent_find( c, strict_greater) == c.end() )
      return true;
  }
  
  return false;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
