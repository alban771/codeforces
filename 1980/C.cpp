#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n, m, k;
  cin >> n;
  vector<ull> a(n), b(n);
  multiset<ull> d;
  cin >> a >> b >> m;
  if ( m > 1 )
    copy_n( istream_iterator<ull>(cin), m-1, inserter(d, d.begin()) );
  cin >> k;
  d.insert(k);

  cerr << "n: " << n << " m: " << m << endl;
  cerr << "a: [" << a << "]" << endl;
  cerr << "b: [" << b << "]" << endl;
  cerr << "d: [";
  rn::copy(d, ostream_iterator<ull>(cerr, " "));
  cerr << "]" << endl;


  if ( rn::find(b,k) == b.end() )
    return false;


  vector<ull> c;
  for( int i = 0; i < n; ++i ) {
    if ( a[i] != b[i] ) {
      c.push_back(b[i]); 
    }
  }
  rn::sort(c);

  auto first_b = c.begin(); 
  auto first_d = d.begin();
  while( first_b != c.end() && first_d != d.end() ){
    if ( *first_b < *first_d ) return false;
    else if ( *first_b > *first_d ) ++first_d;
    else {
      ++first_b;
      ++first_d;
    }
  }

  return first_b == c.end();
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
