#include <bits/stdc++.h>
using namespace std;
using ull = unsigned long long;
namespace rn = std::ranges;
namespace vw = std::ranges::views;


template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

void print( const int v ){}


constexpr ull M = 1e14;


auto solve() {
  int n,q;
  cin >> n >> q;
  vector<ull> c(n);
  cin >> c;
  vector<pair<ull,int>> v(n);
  rn::transform(c, v.begin(), [r=0,acc=0ull](ull e) mutable {
      acc += e;
      r += (acc > M);
      acc %= M;
      return pair{acc, r};
    });

//  for ( auto [x,_] : v ) cerr << x << " ";
//  cerr << endl;
  for( int _ = 0; _ < q; ++_ )
  {
    int f, l;
    cin >> f >> l;
//    cerr << f << "-" << l;
    int d = (l-f+1) + (l-f+2)/2;
//    cerr << " d: " << d;
    ull r = v[l-1].first - (f > 1 ? v[f-2].first : 0);
    int k = v[l-1].second - (f > 1 ? v[f-2].second : 0);
    if ( k ) r += M;
//    cerr << " r: " << r << endl;
    print( f != l && r >= d );
  }
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    solve();
  }
}
