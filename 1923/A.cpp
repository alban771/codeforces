#include <bits/stdc++.h>
using namespace std;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n,c;
  cin >> n;
  vector<bool> a(n);
  char b;
  int f{n},l{};
  for( int i = 0; i < n; ++i )
  {
    cin >> b;
    cin.ignore();
    a[i] = b == '1';
    if ( a[i] ) {
      f = min(f,i);
      l = max(l,i);
    }
  }

  int k{},rv{};
  for( int i = l; i > f ; --i )
  {
    if ( !a[i] ) {
      ++k;
    }
  }

  return k;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
