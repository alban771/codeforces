#include <bits/stdc++.h>
using namespace std;
using ull = unsigned long long;
namespace rn = std::ranges;
namespace vw = std::ranges::views;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  ull n, k;
  cin >> n >> k;
  vector<ull> a(n);
  vector<int> x(n);
  cin >> a >> x;
  vector<pair<int,ull>> v(n);
  rn::transform( a, x, v.begin(),
      [](ull ai, int xi){ return pair{xi > 0 ? xi : -xi, ai}; });
  rn::sort(v);
//  cerr << "k: " << k << endl;
  return !rn::any_of( v, [k,t=0,b=0]( auto p ) mutable {
      auto [xi,ai] = p;
      if ( b < ai ){
        t += (ai-b-1) / k + 1;
        b = (ai-b) % k;
        b = b == 0 ? 0 : k-b;
      }
      else {
        b -= ai;
      }
//      cerr << " m: " << ai << "-" << xi;
//      cerr << " t: " << t << " b: " << b;
//      cerr << endl;
      return t > xi;
    });
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
