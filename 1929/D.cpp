#include <bits/stdc++.h>
using namespace std;
using ull = unsigned long long;
namespace rn = std::ranges;
namespace vw = std::ranges::views;


template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }


constexpr ull M = 998244353;
vector<vector<int>> al;
using set_t = tuple<int,int,int>; /*current,next,size*/

set_t up_stream( set_t s ) {
  auto& [a,b,k] = s;
  while ( al[b].size() == 2 ) {
    ++k;
    swap(b,a);
    b = al[a][b == al[a][0]];
  }
  return s;
}


ull good_set( vector<set_t> q );


ull merge_comb_set( const vector<set_t>& cst, vector<set_t>& comb ) {


  ull rv {};
  set_t s;
  for( int i = 0; i < comb.size(); ++i )
  {
    s = comb[i];
    comb.erase(comb.begin() + i);
    vector<set_t> m(cst.size() + comb.size());
    rn::merge( cst, comb, m.begin() );
    rv += good_set(move(m));
    rv %= M;
    rv += merge_comb_set(cst, comb);
    rv %= M;
    comb.insert(comb.begin() + i, s);
  }

  return rv;
}


ull good_set( vector<set_t> q ) {

  if ( q.size() < 3 ) return 0ull;

//  cerr << "q: ";
//  for ( auto [a,b,k] : q )
//    cerr << a << "-" << b << "-" << k << " ";
//  cerr << endl;

  ull rv = accumulate( q.begin(), q.end(), 1ull,
      [](ull ss, set_t e){ return (ss * get<2>(e)) % M; });

//  cerr << "r: " << rv << endl;

  /* merge sets */
  vector<set_t> m;
  for ( auto f = q.begin(), g = f; f != q.end(); f=g ) {
    int r = get<1>(*f);
    g = rn::find_if( rn::subrange(f,q.end()), [r](int e){ return e != r; },
        [](auto s){ return get<1>(s); });
    if ( g-f == al[r].size() - 1 ) {
      int b = *rn::find_if(al[r], [ng=rn::subrange(f,g)](int e){
            return rn::find( ng, e, [](auto s){ return get<0>(s); } ) == ng.end(); });
      m.emplace_back( r, b, 1);
//          accumulate(f, g, 1, [](int s, auto p){ return s + get<2>(p); }));
    }

  }

//  cerr << "m: ";
//  for ( auto [a,b,k] : m )
//    cerr << a << "-" << b << "-" << k << " ";
//  cerr << endl;


  for ( set_t s : m ) {
    auto [a,b,k] = s;
    s = up_stream(s);

    vector<int> va = al[a];
    rn::sort(va);
    vector<set_t> g, qa;
    rn::set_intersection( q, va | vw::transform([](int k){ return set_t{k,0,0}; }),
        back_inserter(qa), {}, [](auto p){return set_t{get<0>(p),0,0};} );
    rn::set_difference( q, qa, back_inserter(g) );

    rv += merge_comb_set( g, qa );
    rv %= M;

    g.insert( rn::lower_bound(g, s), s);

    rv += good_set( move(g) );
    rv %= M;

  }

  return rv;
}


auto solve() {
  int n;
  cin >> n;
  al.clear();
  al.resize(n+1);
  for ( int i = 0; i < n-1; ++i ) {
    int u,v;
    cin >> u >> v;
    al[u].push_back(v);
    al[v].push_back(u);
  }

  ull rv = 1 + n*(n+1) / 2; // s0 + s1 + s2
  rv %= M;

  vector<set_t> q;
  for( int i = 0; i < n+1; ++i )
  {
    if( al[i].size() == 1 )
      q.emplace_back(i,al[i][0],1);
  }
  for ( auto& s : q )
    s = up_stream(s);

  rn::sort(q);

  return (rv + good_set( move(q) )) % M;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
