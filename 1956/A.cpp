#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vs = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  if ( v.size() ) 
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int k,q;
  cin >> k >> q;
  vector<int> a(k), n(q);
  cin >> a >> n;
  rn::transform(n, n.begin(), [m=a[0]-1](int e){ return min(e,m); });
  return n;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}

