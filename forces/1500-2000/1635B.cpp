#include <bits/stdc++.h>
using namespace std;





int main() {


  int t, n;
  cin >> t;
  for( int i = 0; i < t; ++i )
  {
    cin >> n;
    vector<int> v(n);
    copy_n( istream_iterator<int>(cin), n, v.begin());

    int res = 0;
    for( int i = 1; i < n-1; ++i )
    {
      if ( v[i] > v[i-1] && v[i+1] < v[i] ){
        v[i] = max( v[i-1], v[i+1]);
        ++res;
      }
    }

    cout << res << endl;
    copy( v.begin(), v.end(), ostream_iterator<int>(cout, " ") );
  }




  

}
