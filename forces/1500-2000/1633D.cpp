/*
 * You have an array of integers a of size n. Initially, all elements of the array are equal to 1. You can perform the following operation: choose two integers i (1≤i≤n) and x (x>0), and then increase the value of ai by ⌊aix⌋ (i.e. make ai=ai+⌊aix⌋

).

After performing all operations, you will receive ci
coins for all such i that ai=bi

.

Your task is to determine the maximum number of coins that you can receive by performing no more than k
operations.
*/


#include <bits/stdc++.h>
using namespace std;


int INF = 1e7;


int steps( int a, int c ) {
  if ( a > c ) return INF;

  vector<pair<int,int>> l(c+1, {INF, -1});
  l[a] = {0, a};
  for( int i = a; i < c; ++i ) {
    if ( l[i].first < 0 ) continue;
    for ( int x = 1; x <= i; ++x ) {
      int k = i + i/x;
      if( i < k && k <= c ) 
        if (l[i].first + 1 < l[k].first)
          l[k] = {l[i].first + 1, i};
    }
  }

  cout << c << "-" << a << ": ";
  while ( c > a ){
    cout << c << " ";
    c = l[c].second;
  }
  cout << a << endl;

  
  return l.back().first;
}



int coin( int n, int k, vector<int>& a, vector<int>& c) {
  
  vector<int> l(n);
  for( int i = 0; i < n; ++i )
    l[i] = steps( a[i], c[i] );

  
  cerr << " k : " << k << endl; 

  cerr << " l : [ ";
  copy( l.begin(), l.end(), ostream_iterator<int>(cerr, " ") );
  cerr << "]" << endl;

  cerr << " c : [ ";
  copy( c.begin(), c.end(), ostream_iterator<int>(cerr, " ") );
  cerr << "]" << endl;



  int res = 0;
  vector<int> chosen;
  function<void(int)> search = [&]( int i ) -> void {

    if ( i == n ){
      res = max(res,  accumulate( chosen.begin(), chosen.end(), 0, 
            [&]( int s, int ii ){
              return s + c[ii];
            }));
      return;
    }

    if ( k >= l[i] ) {
      chosen.push_back(i);
      k -= l[i];
      search( i+1 );
      k += l[i];
      chosen.pop_back();
    } 
    search( i+1 );
  };

  search( 0 );

  return res;
}




int main() {
  int ntest;
  cin >> ntest;
  for( int i = 0; i < ntest; ++i )
  {
    int n, k;
    cin >> n >> k;
    vector<int> a(n), c(n);
    copy_n( istream_iterator<int>(cin), n, a.begin() );
    copy_n( istream_iterator<int>(cin), n, c.begin() );
    cout << coin(n, k, a, c) << endl;
  }
}
