/* You've got array a[1], a[2], ..., a[n], consisting of n integers. Count the number of ways to split all the elements of the array into three contiguous parts so that the sum of elements in each part is the same. 
 */

#include <bits/stdc++.h>
using namespace std;


int main() {

  size_t n;
  cin >> n;
  vector<int> v(n);
  generate( v.begin(), v.end(), 
    [](){ 
      int k;
      cin >> k;
      return k; 
    });


  unsigned long long res = 0;
  long long s1 = 0ll; 
  long long S = accumulate(v.begin(), v.end(), 0ll); 

  if ( S % 3 != 0 ) { 
    cout << 0 << endl;
    return 0;
  }
  S /= 3;

  vector<int> i1;
  for( int i = 0; i < n-2; ++i )
  {
    s1 += v[i];
    if ( s1 == S ) i1.push_back(i+1);
  }

  s1 = 0;
  for( int j = n-1; j > 1; --j )
  {
    s1 += v[j];
    if ( s1 == S ) {
      res += lower_bound(i1.begin(), i1.end(), j) - i1.begin();
    }
  };

  cout << res << endl;
}
