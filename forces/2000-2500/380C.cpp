#include <bits/stdc++.h>


using namespace std;

vector<int> v_(1e6 + 2);

int main() {

  string s;

  int t;
  cin >> s >> t;
  int l = 0; 
  for( int i = 0; i < s.size(); ++i )
  {
    v_[i] = l;
    l += s[i] == '(' ? 1 : -1;
  }
  v_[s.size()] = l;


  while( --t >= 0 ){
    int u, v;
    cin >> u >> v;
    --u;
    int a = v_[u];
    for( int i = u; i <= v; i += 1 + v_[i] - a ) {
      a = min(a, v_[i]);
    }
    cout <<  v - u - (v_[u] - a) - ( v_[v] - a ) << endl;  
  }

}





//   \ / \ \ / / / / / /
