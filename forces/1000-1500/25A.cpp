/*
 *
 * Bob is preparing to pass IQ test. The most frequent task in this test is to find out which one of the given n numbers differs from the others. Bob observed that one number usually differs from the others in evenness. Help Bob — to check his answers, he needs a program that among the given n numbers finds one that is different in evenness.
 */



#include <bits/stdc++.h>

using namespace std;


int main() {

  int a,b,c;
  cin >> a >> a >> b >> c; 
  if( (a + b) % 2 )  {
    if ( (a+c) % 2 )
      cout << 1 << endl;
    else 
      cout << 2 << endl;
  }
  else
  {
    b = 2;
    a %=2;
    while( ++b, (c % 2) == a ) cin >> c;
    cout << b << endl;
  }
}

