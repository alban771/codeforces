/* 
 * After the lessons n groups of schoolchildren went outside and decided to visit Polycarpus to celebrate his birthday. We know that the i-th group consists of si friends (1 ≤ si ≤ 4), and they want to go to Polycarpus together. They decided to get there by taxi. Each car can carry at most four passengers. What minimum number of cars will the children need if all members of each group should ride in the same taxi (but one taxi can take more than one group)?
 *
 *
 */

#include <iostream>
#include <vector>
#include <algorithm>


using namespace std;

int main() {

  int n;
  cin >> n;
  vector<int> s(n), p(4);
  for ( int& k : s ) cin >> k;

  for ( int i =0; i <4; ++i )
    p[i] = count(s.begin(),s.end(), i+1);

  p[0] = max(0, p[0] - p[2] - 2*(p[1]%2));

  cout << ( p[3] + p[2] + (int)((p[1]+1)/2) + 
            ( p[0] == 0 ? 0 : (int)((p[0]+3)/4))  )  << endl;
  

}
