/*
 *
 * You have a positive integer m and a non-negative integer s. Your task is to find the smallest and the largest of the numbers that have length m and sum of digits s. The required numbers should be non-negative integers written in the decimal base without leading zeroes.
 */


#include <bits/stdc++.h>
using namespace std;

int main() {

  int m,s;
  cin >> m >> s;


  if ( s == 0 )
    if ( m == 1 )
      cout << 0  << " " << 0 << endl;
    else
      cout << -1 << " " << -1 << endl;
  else if ( s > 9*m )
    cout << -1 << " " << -1 << endl;
  else {
    int k = s/9, p = s%9;
    stringstream M, N;
    M << string( k, '9' );
    if ( k < m ) {
      M << p;
      M << string( m-k-1, '0' );
    }

    if ( p == 0 ) {
      k-=1;
      p=9;
    }

    if ( k == m-1 )
      N << p;
    else if ( k < m-1 ){
      N << 1;
      N << string(m-k-2, '0');
      N << p-1;
    }
    N << string(k, '9');

    cout << N.str() << " " << M.str() << endl;
  }
  

}
