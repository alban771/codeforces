#include <iostream>
using namespace std;
int main() {
  int x,y,s;
  cin >> x >> y >> s;

  int d = abs(x) + abs(y);

  cout << ( d > s || (s-d) % 2 == 1 
      ?  "No" : "Yes" ) << endl;

}
