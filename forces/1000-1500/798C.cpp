#include <bits/stdc++.h>
using namespace std;

/* 1250
 *
 *
 * C. Infected Tree
time limit per test3 seconds
memory limit per test256 megabytes
inputstandard input
outputstandard output
Byteland is a beautiful land known because of its beautiful trees.

Misha has found a binary tree with n vertices, numbered from 1 to n. A binary tree is an acyclic connected bidirectional graph containing n vertices and n−1 edges. Each vertex has a degree at most 3, whereas the root is the vertex with the number 1 and it has a degree at most 2.

Unfortunately, the root got infected.

The following process happens n times:

Misha either chooses a non-infected (and not deleted) vertex and deletes it with all edges which have an end in this vertex or just does nothing.
Then, the infection spreads to each vertex that is connected by an edge to an already infected vertex (all already infected vertices remain infected).
As Misha does not have much time to think, please tell him what is the maximum number of vertices he can save from the infection (note that deleted vertices are not counted as saved).

Input
There are several test cases in the input data. The first line contains a single integer t (1≤t≤5000) — the number of test cases. This is followed by the test cases description.

The first line of each test case contains one integer n (2≤n≤3⋅105) — the number of vertices of the tree.

The i-th of the following n−1 lines in the test case contains two positive integers ui and vi (1≤ui,vi≤n), meaning that there exists an edge between them in the graph.

It is guaranteed that the graph is a binary tree rooted at 1. It is also guaranteed that the sum of n over all test cases won't exceed 3⋅105.

Output
For each test case, output the maximum number of vertices Misha can save.

*/


template<typename T, typename U>
istream& std::operator>>( istream& in, pair<T,U>& p ){
  return in >> p.first >> p.second;
}

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n-1);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}


template<typename T, size_t N>
struct tuple_printer_t {
  static ostream& print( ostream& out, const T& tuple ){
    return tuple_printer_t<T,N-1>::print(out, tuple) << ", " << get<N>(tuple);
  }
};

template<typename T>
struct tuple_printer_t<T,0> {
  static ostream& print( ostream& out, const T& tuple ){
    return out << get<0>(tuple);
  }
};

template<typename... A>
ostream& std::operator<<( ostream& out, const tuple<A...> v ){
  return tuple_printer_t<tuple<A...>, sizeof...(A)-1>::print(out, v);
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}


void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());  
  if ( v.size() ) 
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }



template<typename T>
struct apair : public pair<T,T>{
  void swap(){ swap( pair<T,T>::first, pair<T,T>::second ); }
};

template <typename T, uint16_t N> struct _ntuple {
  using type = decltype(tuple_cat(typename _ntuple<T, N / 2>::type{},
                                  typename _ntuple<T, N / 2>::type{}));
};

template<typename T>
struct _ntuple<T, 1>{ 
  using type = tuple<T>;
};

template<typename T, uint16_t N>
using ntuple = typename _ntuple<T,N>::type;




ntuple<int,2> d[30'005] {};
auto comp_d = []( const apair<int>& lhs, const apair<int>& rhs) {
      return get<0>(d[lhs.first]) < get<0>(d[rhs.first]); };

int calc( vector<apair<int>>::iterator ei, 
    const vector<apair<int>>::iterator ee) {
  if ( ei == ee ) return 0;

  auto en = upper_bound(ei,ee,*ei, comp_d);
  auto em = upper_bound(en,ee,*en, comp_d);

  const int p = ei->first;
  vector<int> m, s;
  for( ; ei != en; ++ei ){
    if( ei->first != p ) continue;
    m.push_back(calc(find_if(en, em, 
          [ei](apair<int> e){ return e.first == ei->second; }), ee));
    s.push_back( get<1>(d[ei->second]) );
  }

  if (!m.size()) return 0;

  transform(m.begin(), m.end(), s.begin(), m.begin(), 
    [k=accumulate(m.begin(), m.end(), 0)]( const auto c, const auto f ){ 
      return k + f - c; 
  });

  return *max_element( m.begin(), m.end() );
};


vector<int> bfs( const vector<vector<int>>& adj, int root, size_t n ){
    vector<bool> vis(n, false); 
    vector<int> d(n);
    queue<int> q;
    q.push(root);
    d[root] = 0;
    vis[root] = true;

    while ( q.size() ) {
      int s = q.front(); q.pop();
      for( int e : adj[s] ) {
        if ( vis[e] ) continue;
        q.push(e);
        vis[e] = true;
        d[e] = d[s] + 1;
      }
    }
    return d;
}


auto solve() {
  vector<apair<int>> el;
  cin >> el;

  {
    vector<vector<int>> adj(el.size()+2);
    for( auto [u,v] : el ) {
      adj[u].push_back(v);
      adj[v].push_back(u);
    }

    vector<int> d_ = bfs(adj, 1, el.size()+2);
    for( int i = 1; i < el.size()+2; ++i )
    {
      get<0>(d[i]) = d_[i];
      get<1>(d[i]) = 0;
    }
  }

  for( auto& [u,v] : el ) { 
      if ( get<0>(d[u])>get<0>(d[v]) ) swap(u, v);
  }
  sort( el.begin(), el.end(), comp_d);

  for_each(el.rbegin(), el.rend(), [&]( auto e ) -> void {
    auto [u,v] = e; 
    get<1>(d[u]) += 1 + get<1>(d[v]);
  });

  return calc( el.begin(), el.end() );
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
