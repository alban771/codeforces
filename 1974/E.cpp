#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }



using dp_t = map<int,ull>;

dp_t max_happiness( const vector<tuple<ull,int>>& ch, ull x, int m ) {
  if ( m == ch.size() ) 
    return {{0, 0}};

  dp_t n = max_happiness( ch, x, m+1 );
  dp_t rv;
  rn::for_each(n, [x](auto& e){ 
      ull &ci = get<1>(e);
      ci = ci > x ? ci - x : 0;
  });

  const auto [c,h] = ch[m];

  ull b = m*x; 
  if ( b >= c ) {
    for ( const auto& [hi,ci] : n ) {
      if ( b >= c+ci ) {
        rv[h+hi] = c+ci;
      }
    }
  }

  for( auto it_n = n.begin(), it_r = rv.begin();
       it_n != n.end() && it_r != rv.end(); ) {
    if ( it_n->first < it_r->first ) {
      ++it_n;
    }
    else if ( it_n->first > it_r->first ) {
      ++it_r;
    }
    else {
      it_r->second = min(it_r->second, it_n->second);
      ++it_n;
      ++it_r;
    }
  }

  rv.merge(n);
  return rv;
}


auto solve() {
  int m;
  ll x;
  cin >> m >> x;
//  cerr << endl << "--" << endl;
//  cerr << "m: " << m << " x: " << x << endl;
  vector<tuple<ull,int>> ch(m);
  for(int i = 0; i<m; ++i ) {
    auto& [c,h] = ch[i];
    cin >> c >> h;
  }

  dp_t rv = max_happiness( ch, x, 0 );
  return rv.rbegin()->first;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
