#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }
 
template<typename T>
using apair = pair<T,T>;

auto solve() {
  vector<int> a;
  cin >> a;
  map<apair<int>,multiset<int>> d[3];
  for( int i = 0; i < a.size()-2; ++i ) {
    d[0][pair{a[i], a[i+1]}].insert(a[i+2]);
    d[1][pair{a[i], a[i+2]}].insert(a[i+1]);
    d[2][pair{a[i+1], a[i+2]}].insert(a[i]);
  }
  ll rv = 0;
  for( int i = 0; i < 3; ++i ) {
    for( auto &ms : d[i]) {
      int m=-1;
      ll a = 0,b = 0;
      for ( auto e : ms.second ) {
        assert( e >= m );
        if ( e == m )
          ++b;
        else {
          m = e;
          a += b;
          b = 1;
        }
        rv += a;
      }
    }
  }
  return rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
