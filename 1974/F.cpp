#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }

template<typename T> using apair = pair<T,T>;
using coord_t = vector<apair<ull>>;

ull lim[4];

template<typename T, typename C>
int chips( T& r, C comp, ull b, bool p ) {
  auto it = rn::upper_bound(r, pair{b, numeric_limits<ull>::max()}, comp); 
//  cerr << "b: " << b << " list: ";
  int rv = count_if( r.begin(), it, [a=lim[!p], b=lim[2+!p]]( auto p ){ 
//      cerr << p.first << "-" << p.second << " ";
      return (a < p.second) & (p.second <= b);
      });
//  cerr << endl;
//  cerr << "rv: " << rv << endl;
//  r = r.base() | vw::drop(it-r.base().begin());
  r.begin() = it;
//  cerr << "r.size: " << r.size() << endl;
  return rv;
}


auto solve() {
  lim[0] = 0;
  lim[1] = 0;
  cin >> lim[2] >> lim[3];
  int n, m;
  cin >> n >> m;
  coord_t r(n), c(n);
  for( int i = 0; i < n; ++i ) {
    ll u,v;
    cin >> u >> v;
    r[i] = {u,v};
    c[i] = {v,u};
  }
  rn::sort(r);
  rn::sort(c);

  vector<int> rv{0,0};
  auto b = vector{ vw::all(r), vw::all(c) };
  auto e = vector{ vw::reverse(r), vw::reverse(c) };
  for( int i = 0; i < m; ++i )
  {
    char h;
    ll k;
    cin >> h >> k;
    if ( "UL"sv.find(h) != string_view::npos ) {
      bool p = "UL"sv.find(h);
      lim[p] += k;
      rv[ i % 2 ] += chips(b[p],less{},lim[p],p);
    }
    else {
      bool p = "DR"sv.find(h);
      lim[2+p] -= k;
      rv[ i % 2 ] += chips(e[p],greater{},lim[2+p],p);
    }
  }

  return rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
