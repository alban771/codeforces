#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, ""));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  if ( v.size() )
    cout << v << endl;
  else
    cout << "NO" << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  vector<char> is;
  cin >> is;
  vector<char> rv;
  int dd[2]{}; 
  bool h {};

  for ( char c : is ) {
    int &d = dd["NSEW"sv.find(c) / 2];
    int s = "SW..NE"sv.find(c) / 2 - 1;
    if ( (d * s < 0) | ( (d == 0) & h ) ) {
      rv.push_back('R');
      d += s;
    }
    else {
      h = true;
      rv.push_back('H');
      d -= s;
    }
  }

  if ( (dd[0] != 0) | (dd[1] != 0) || rn::find(rv, 'R') == rv.end() )
    rv.clear();
  return rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
