#include <bits/stdc++.h>
using namespace std;
using ui = unsigned int;
namespace rn = std::ranges;
namespace vw = std::ranges::views;


template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }

constexpr ui ones = (1u << 31) - 1u;

auto solve() {
  vector<ui> a;
  cin >> a;
  rn::sort(a);
  int rv = 0;
  for( int i = 0, j = a.size()-1 ; i < j; )
  {
    ui k = a[i]+a[j];
    if ( k > ones ) --j;
    else if ( k < ones ) ++i;
    else {
      ++rv;
      ++i;
      --j;
    }
  }
  return a.size() - rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
