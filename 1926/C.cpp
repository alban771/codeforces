#include <bits/stdc++.h>
using namespace std;
using ull = unsigned long long;
namespace rn = std::ranges;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }


int unit[10] = {0, 1, 3, 6, 10, 15, 21, 28, 36, 45 };

auto solve() {
  string n;
  cin >> n;
  int rv {};
  int k = n.size();
  int acc = 0;
  for ( char a : n ) {
    --k;
    rv += 45 * k * pow(10, k-1) * (a - '0');
    rv += unit[max(a-'0'-1,0)] * pow(10, k) + (a-'0');
    rv += (a-'0') * pow(10,k) * acc;
    acc += a - '0';
  }
  return rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
