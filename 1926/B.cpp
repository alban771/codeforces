#include <bits/stdc++.h>
using namespace std;
namespace rn = std::ranges;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "TRIANGLE": "SQUARE" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  vector<bitset<11>> g;
  cin >> g;
  int m = rn::minmax( g, less<int>{}, []( bitset<11> r ){ return r.count(); }).max.count();
  return rn::any_of( g, [m]( bitset<11> r ){ return r.count() > 0 && r.count() != m; } );
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}

