#include <bits/stdc++.h>
using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ull = unsigned long long;
template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "Anna": "Sasha" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n,m;
  cin >> n >> m;
  vector<string> a(n);
  cin >> a;

  for ( auto& s : a ) rn::reverse(s);
  rn::sort(a);

//  for( auto& s : a ) cerr << s << " ";
//  cerr << endl;

  int i = 0;
  int rv = 0;
  for ( auto& s : a ) {
    if ( ++i % 2 ) {
      rv += s.end() - rn::find_if(s, [](char c){ return c != '0'; });
    }
    else {
      rv += s.size();
    }
  }

//  cerr << "rv: " << rv << endl;

  return rv <= m;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
