#include <bits/stdc++.h>
using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ull = unsigned long long;

template<typename T>
using apair = pair<T,T>;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  ull n,x,y;
  cin >> n >> x >> y;
//  cerr << "x: " << x << " y: " << y << endl;
  vector<ull> a(n);
  cin >> a;

  vector<apair<ull>> v(n);
  transform( a.begin(), a.end(), v.begin(), [x,y]( ull ai ) { return pair{ ai%x, ai%y };});
  rn::sort(v);
//  for ( auto [u,v] : v ) {
//    cerr << u << "-" << v << " ";
//  }
//  cerr << endl;

  auto f = v.begin(), l = v.end() - 1;
  ull k;
  ull rv = 0;
  while( f != l ) {
    k = f->first + l->first;
    if ( k == x ) {
      for (auto g = l; g->first == l->first && g != f; --g ) {
        rv += g->second == f->second;
      }
    }

    if ( k > x ) --l;
    else ++f;
  }


  rn::adjacent_find(v, [&rv, b=0ull](auto lhs, auto rhs) mutable {
      if ( rhs.first != 0 ) return true;
      if ( lhs.second == rhs.second ) {
        rv += ++b;
      }
      else {
        b = 0;
      }
      return false;
      });

  return rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
