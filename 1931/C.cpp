#include <bits/stdc++.h>
using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  size_t n;
  in >> n;
  v.resize(n);
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }

using ull = unsigned long long;



auto solve() {
  vector<ull> a;
  cin >> a;
  auto f = rn::find_if(a | vw::drop(1), [k=a[0]](ull ai){ return ai != k; }) - a.begin();
  auto l = rn::find_if(a | vw::reverse  | vw::drop(1), [k=a.back()](ull ai){ return ai != k; }) - a.rbegin();
  size_t d = a[0] == a.back() ? f + l : max(f,l);
  return d > a.size() ? 0 : a.size() - d;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
