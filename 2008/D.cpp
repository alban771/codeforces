#include <bits/stdc++.h>
using namespace std;
namespace rn = std::ranges;

template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  if ( v.size() == 0 ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
//  print((bool) v.size());
//  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n;
  cin >> n;
  vector<int> p(n), f(n,-1), cc;
  string c;
  int acc {};
  cin >> p >> c;
  rn::transform(p, p.begin(), []( const int e ){ return e-1; });

  for( int i = 0; i < n; ++i, cc.clear(), acc=0 )
  {
    if ( f[i] > -1 ) continue;
    int j = i;
    do {
      cc.push_back(j);
      acc += '1'-c[j];
      j = p[j];
    } while (  i != j );
    for ( int cl : cc ) {
      f[cl] = acc;
    }
  }
  return f;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
