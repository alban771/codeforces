#include <bits/stdc++.h>
using namespace std;
namespace rn = std::ranges;


template<typename T>
istream& std::operator>>( istream& in, vector<T>& v ){
  if ( v.size() == 0 ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}


template<>
istream& std::operator>>( istream& in, vector<bool>& v ){
  if ( v.size() == 0 ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  char c;
  for( int i = 0; i < v.size(); ++i )
  {
    cin >> c;
    v[i] = c == '1';
  }
  return in;
}


template<typename T>
ostream& std::operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  vector<bool> v;
  cin >> v;
  int n = 1;
  while ( n < 450 && n * n != v.size() ) ++n;
  if ( n == 450 ) return false;
  if ( n < 3 ) return rn::find(v, false) == v.end();
  return rn::find(v, false) == v.begin() + n + 1;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
