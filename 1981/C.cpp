#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }


const auto pos = [](ll e){ return e >= 0; };

vector<ll> solve() {
  vector<ll> a;
  cin >> a;
  auto f = a.begin(), g = find_if(f+1, a.end(), pos);

  for(; g != a.end(); f = g, g = find_if(f+1, a.end(), pos) ) {
    ll u = *f, v = *g;
    if ( u == -1 ) continue;
    auto a = f, b = g;
    while ( u != v && a != b ) {
      if ( u > v ) {
        u >>= 1;
        *++a = u; 
      }
      else {
        v >>= 1;
        *--b = v;
      }
    }
    if ( u != v || (b-a) ^ 1 )
      return {-1l};
    generate(a,b,[u,i=0]() mutable { return (++i & 1) ? 2*u : u; });
  }
  
  return a;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
