#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<T>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n,m,sz;
  cin >> n >> m;
  sz = n+m+1;
  vector<ull> a(sz),b(sz),rv(sz); 
  cin >> a >> b;
  vector<bool> p(sz, false);
  int cn = n+1, cm = m+1;
  ull s = 0;
  for( int i = 0; i < sz; ++i ) {
    if ( a[i] < b[i] ) {
      --cm;
      p[i] = cm < 0;
    } else {
      --cn;
      p[i] = cn >= 0;
    }
    s += p[i] * a[i] + !p[i] * b[i];
  }
  assert( cn > 0 || cm > 0 );
  if ( cm > 0 ) {
    int i = sz - 1 - (rn::find(p | vw::reverse, true) - p.rbegin());
    ull d = a[i]-b[i];
    rn::transform(a,b,rv.begin(), [s,d,cn=n](ull a, ull b) mutable {
        if ( a > b && --cn >= 0 )
          return s - a;
        else 
          return s - d - b;
    });
  }
  else {
    int i = sz - 1 - (rn::find(p | vw::reverse, false) - p.rbegin());
    ull d=b[i]-a[i];
    rn::transform(a,b,rv.begin(), [s,d,cm=m](ull a, ull b) mutable {
        if ( b > a && --cm >= 0 )
          return s - b;
        else 
          return s - d - a;
    });
  }
  return rv;
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
