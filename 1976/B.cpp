#include <bits/stdc++.h>

using namespace std;
namespace rn = std::ranges;
namespace vw = std::ranges::views;
using ll = long long;
using ull = unsigned long long;

template<typename T>
istream& operator>>( istream& in, vector<T>& v ){
  if ( !v.size() ) {
    size_t n;
    in >> n;
    v.resize(n);
  }
  copy_n( istream_iterator<T>(in), v.size(), v.begin());
  return in;
}

template<typename T>
ostream& operator<<( ostream& out, const vector<T>& v ){
  copy( v.begin(), v.end(), ostream_iterator<int>(out, " "));
  return out;
}

void print( const bool v ){ cout << ( v ? "YES": "NO" ) << endl; }

template<typename T>
void print( const vector<T>& v ){
  print((bool) v.size());
  if ( v.size() )
    cout << v << endl;
}

template<typename T>
void print( const T v ){ cout << v << endl; }




auto solve() {
  int n;  
  cin >> n;
  vector<ull> a(n),b(n+1),d(n);
  cin >> a >> b;
  
  rn::transform(a, b, d.begin(), 
      [k=b.back()](ull lhs, ull rhs){
        auto [n,x] = minmax(lhs,rhs);
        return k > x ? k - x : k < n ? n - k : 0;
      });
  b.back() = *rn::minmax_element(d).min + 1;
  rn::transform(a, b, b.begin(), 
      [](ull lhs, ull rhs){ return lhs > rhs ? lhs - rhs : rhs - lhs; });
  return reduce(b.begin(), b.end(), 0ull);
}



int main(){
  ios::sync_with_stdio(false);
  int t;
  cin >> t;
  for( int i = 0; i < t; ++i ){
    print( solve());
  }
}
